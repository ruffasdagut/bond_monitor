BEGIN;

DROP TABLE bond_series;

CREATE TABLE bond_series
(
    id INTEGER PRIMARY KEY,
    name TEXT NOT NULL,
    series TEXT NOT NULL
);
INSERT INTO bond_series (name,series) VALUES ("Series I","I");
INSERT INTO bond_series (name,series) VALUES ("Series E","E");
INSERT INTO bond_series (name,series) VALUES ("Series EE","N");
INSERT INTO bond_series (name,series) VALUES ("Savings Notes","S");


DROP TABLE savings_bond_interest_data;
CREATE TABLE savings_bond_interest_data 
(
    series_id INTEGER REFERENCES bond_series(id),
    redemption_year INTEGER,
    redemption_month INTEGER,
    issue_year INTEGER,
    issue_month INTEGER,
    value float
);

DROP TABLE savings_bond;
CREATE TABLE savings_bond
(
   id INTEGER PRIMARY KEY,
   series_id INTEGER REFERENCES bond_series(id),
   serial_number TEXT,
   issue_year INTEGER,
   issue_month INTEGER,
   value INTEGER
);
#INSERT INTO savings_bond (series_id,serial_number,issue_year,issue_month,value) VALUES(3,'L511931034EE',1993,06,50);
#INSERT INTO savings_bond (series_id,serial_number,issue_year,issue_month,value) VALUES(3,'L511978199EE',1993,07,50);
#INSERT INTO savings_bond (series_id,serial_number,issue_year,issue_month,value) VALUES(3,'L512030802EE',1993,08,50);
#INSERT INTO savings_bond (series_id,serial_number,issue_year,issue_month,value) VALUES(3,'L512812998EE',1993,09,50);
COMMIT;



SELECT sb.id,sb.serial_number,sbid.value * (sb.value/25)
FROM savings_bond sb
     JOIN savings_bond_interest_data sbid ON (sb.issue_year = sbid.issue_year
                                              AND sb.issue_month = sbid.issue_month
                                              AND sb.series_id = sbid.series_id)
WHERE sbid.redemption_year = 2012 AND redemption_month = 12;

SELECT SUM(sbid.value * (sb.value/25))
FROM savings_bond sb
     JOIN savings_bond_interest_data sbid ON (sb.issue_year = sbid.issue_year
                                              AND sb.issue_month = sbid.issue_month
                                              AND sb.series_id = sbid.series_id)
WHERE sbid.redemption_year = 2012 AND redemption_month = 12;
